#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --time=4:00:00
#SBATCH --mem=16GB

# File with paths to query summary stats
echo $1 > coloc_file.txt
QRY=coloc_file.txt
# File with paths to reference summary stats
REF=ReferenceStats.txt
# Output file
OUT=$2
# Window size for assasing coloc
WIN=250000
# GWAS threshold for loci to asses
THR=5e-6
# Output folder
OUTDIR=./coloc_output
# Path to coloc script
COLOC=./Colocolization.r
# Path to coloc processing script
COLOC_PROC=./ColocProcessing.r
# Path to sample size file
SAMPLE_SIZES=SampleSizesSummaryDatabase.txt
# Path to reference MAF
REF_MAF=ReferenceMAF.txt

# Make the output dir
mkdir -p $OUTDIR

module load R
module load cairo

echo "--------------------------------------------------"
echo "[INFO]    Starting coloc analysis"
echo "--------------------------------------------------"
Rscript $COLOC -q $QRY -r $REF -s $SAMPLE_SIZES -m $REF_MAF -o $OUTDIR/$OUT -t $THR -f

echo "--------------------------------------------------"
echo "[INFO]    Starting post processing"
echo "--------------------------------------------------"
Rscript $COLOC_PROC -i $OUTDIR/$OUT -q $QRY -r $REF -o $OUTDIR -w $WIN -t 0.75